import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/servicios/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  password:string;

  constructor(
    private authService:AuthService,
    public router: Router
    ) { }

  ngOnInit() {
  }

  //funcion d elogin
  OnSubmitLogin(){
    //mandamos a llamar la funcion d enuestor servicio 
    this.authService.login(this.email,this.password).then( res => {
      this.router.navigate(['/tabs/tab2']);
    }).catch(err => alert('los datos son incorrectos o no existe el usuario'));
  }

}
