import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private AFaut: AngularFireAuth,
    private router: Router,
    private db: AngularFirestore
    ) { }

  login(email:string,password:string){

    return new Promise((resolve, rejected) =>{

      this.AFaut.signInWithEmailAndPassword(email,password).then(user => {
        resolve(user);
      }).catch(err => rejected(err));

    })


  }

  logout(){
    this.AFaut.signOut().then( auth => {
      this.router.navigate(['/login']);
    });
  }

  register(email:string,password:string,name:string){
    return new Promise((resolve, reject) => {

      this.AFaut.createUserWithEmailAndPassword(email,password).then( res => {
        const uid = res.user.uid;
        this.db.collection('users').doc(uid).set({
          name: name,
          uid: uid

        });
        resolve(res);
      }).catch( err => reject(err));
    });
  }
}
