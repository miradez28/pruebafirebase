import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { message } from '../modelos/message';
import firebase from 'firebase/app';

//modleo de nuestro chat
export interface chat {
  description : string
  name : string
  id :string
  img : string
}

@Injectable({
  providedIn: 'root'
})
export class ChatsService {

  constructor(
    private db:AngularFirestore
  ) { }

  //metodo para obtener los areglos dependiendo de tu coleccion y retornamos los resultados
  getChatRooms(){
    return this.db.collection('chatsRoom').snapshotChanges().pipe(map( rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as  chat;
        data.id = a.payload.doc.id;
        return data;
      })
    }));
  }

  getChatRoom( chat_id : string ){
    return this.db.collection('chatsRoom').doc(chat_id).valueChanges();
  }

  setToMsgToFirebase( message : message, chat_id : string){

    this.db.collection('chatsRoom').doc(chat_id).update({
      messages: firebase.firestore.FieldValue.arrayUnion(message),
    })
  }
}
