import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ChatComponent } from '../paginas/chat/chat.component';
import { AuthService } from '../servicios/auth.service';
import { ChatsService, chat } from '../servicios/chats.service';
import { ActionSheetController } from '@ionic/angular';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  public chatRooms :any = [];

  constructor(
    public authService: AuthService,
    public chatservice:ChatsService,
    public modal: ModalController,
    public actionSheetController: ActionSheetController
  ) {}

  ngOnInit(){
    //mandamos a llamr el servicio y guardamos nuesto resultado en un arreglo
    this.chatservice.getChatRooms().subscribe( chats => {
      this.chatRooms = chats;
    })
  }

  openChat(chat){
    this.modal.create({
      component: ChatComponent,
      componentProps : {
        chat : chat
      }
    }).then ( (modal) => modal.present ())
  }

  Onlogout(){
    this.authService.logout();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Oociones',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Desconectarse',
        role: 'destructive',
        icon: 'log-out',
        handler: () => {
          this.Onlogout();
        }
      }]
    });
    await actionSheet.present();
  }

}
