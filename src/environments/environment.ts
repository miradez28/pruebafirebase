// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

// llaves de firebase
export const firebaseConfig = {
  apiKey: "AIzaSyAN2rlJjJuIspLtfP5S1qvKxFZIv3q9U6w",
  authDomain: "aplicaciontiemporeal.firebaseapp.com",
  databaseURL: "https://aplicaciontiemporeal.firebaseio.com",
  projectId: "aplicaciontiemporeal",
  storageBucket: "aplicaciontiemporeal.appspot.com",
  messagingSenderId: "229844365514",
  appId: "1:229844365514:web:f5fe69da0dc744236fc433",
  measurementId: "G-L5JQBQV03Y"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
